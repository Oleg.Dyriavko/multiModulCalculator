package com.epam.rd.june2018.calculator.core;

import com.epam.rd.june2018.calculator.interfaces.Calc;

public class CalcImpl implements Calc {

    double a;
    double b;
    char op;
    private double result;

    public CalcImpl() {
    }

    public double addition(double a, double b) {
        return calculate(a, b, '+');
    }

    public double subtraction(double a, double b) {
        return calculate(a, b, '-');
    }

    public double multiplication(double a, double b) {
        return calculate(a, b, 'x');
    }

    public double division(double a, double b) {
        return calculate(a, b, '/');
    }

    public double calculate(double a, double b, char op) {
        switch (op) {
            case '+':
                result = a + b;
                break;
            case '-':
                result = a - b;
                break;
            case 'x':
                result = a * b;
                break;
            case '/':
                result = a / b;
                break;
            default:
                System.out.println("Select one of the valid operators: + - x / ");
        }

        return result;
    }

}
