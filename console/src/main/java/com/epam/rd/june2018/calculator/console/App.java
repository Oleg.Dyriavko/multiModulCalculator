package com.epam.rd.june2018.calculator.console;

import com.epam.rd.june2018.calculator.core.CalcImpl;

public class App {

    public static void main(String[] args) {
        double a = Double.parseDouble(args[0]);
        double b = Double.parseDouble(args[1]);
        char op = args[2].charAt(0);
        double result = 0d;

        CalcImpl calc = new CalcImpl();

        switch (op) {
            case '+':
                result = calc.calculate(a, b, '+');
                break;
            case '-':
                result = calc.calculate(a, b, '-');
                break;
            case 'x':
                result = calc.calculate(a, b, 'x');
                break;
            case '/':
                result = calc.calculate(a, b, '/');
                break;
        }

        System.out.println("number1=" + a
                + " number2=" + b
                + " operator=" + op
                + " result=" + result);
    }
}
